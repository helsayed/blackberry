require 'rails_helper'

RSpec.describe "Devices", type: :request do
  describe "DELETE api/v1/devices/:id" do
    let(:device) { create(:device_with_sensors) }

    context "when valid device id" do
      it "delete device with sensors" do
        delete "/api/v1/devices/#{device.id}"

        expect(response).to have_http_status(204)
        expect(Device.all.count).to eq(0)
        expect(Sensor.all.count).to eq(0)
      end
    end
  end
end
