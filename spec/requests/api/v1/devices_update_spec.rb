require 'rails_helper'

RSpec.describe "Devices", type: :request do
  describe "PATCH api/v1/devices" do
    let(:device) { create(:device_with_sensors) }

    let(:valid_device_params){
      { device: {
          sensors_attributes:  [{
            id: device.sensors.first.id,
            name: 'Updated sensor',
            value: Faker::Number.decimal_part(2)
            }
          ]
        }}
    }

    context "when valid params" do
      it "update device with sensors" do
        patch "/api/v1/devices/#{device.id}", params: valid_device_params

        expect(response).to have_http_status(200)
        expect(device.sensors.first.name).to eq('Updated sensor')
        expect(Sensor.all.count).to eq(3)
      end
    end
  end
end
