require 'rails_helper'

RSpec.describe "Devices", type: :request do
  describe "POST api/v1/devices" do
    let(:valid_device_params){
      { device: {
          name: Faker::Name.name,
          category: Faker::Name.name,
          sensors_attributes:  [{
            name: Faker::Name.name,
            value: Faker::Number.decimal_part(2)
            },
            {
              name: Faker::Name.name,
              value: Faker::Number.decimal_part(2)
            }
          ]
        }}
    }

    let(:invalid_device_params){
      { device: {
          category: Faker::Name.name,
          sensors_attributes:  [{
            name: Faker::Name.name,
            value: Faker::Number.decimal_part(2)
            },
            {
              name: Faker::Name.name,
              value: Faker::Number.decimal_part(2)
            }
          ]
        }}
    }

    context "when valid params" do
      it "create device with sensors" do
        post '/api/v1/devices', params: valid_device_params

        expect(response).to have_http_status(201)
        expect(Device.all.count).to eq(1)
        expect(Sensor.all.count).to eq(2)
      end
    end
    context "when invalid params" do
      it "create device with sensors" do
        post '/api/v1/devices', params: invalid_device_params

        expect(response).to have_http_status(422)
        expect(Device.all.count).to eq(0)
        expect(JSON.parse(response.body)).to eq({"name"=>["can't be blank"]})
      end
    end
  end
end
