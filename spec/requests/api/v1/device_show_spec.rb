require 'rails_helper'

RSpec.describe "Devices", type: :request do
  describe "GET api/v1/devices/:id" do
    let(:device) { create(:device_with_sensors) }

    context "when valid device id" do
      it "delete device with sensors" do
        get "/api/v1/devices/#{device.id}"

        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)['name']).to eq(device.name)
        expect(JSON.parse(response.body)['sensors'].count).to eq(device.sensors.count)
      end
    end
  end
end
