FactoryBot.define do
  factory :device do
    name { Faker::Name.name }
    category { Faker::Number.decimal_part(2) }
  end

  factory :device_with_sensors, class: Device do
    name { Faker::Name.name }
    category { Faker::Number.decimal_part(2) }
    after :create do |device_with_sensors|
      create_list :sensor, 3, device: device_with_sensors
    end
  end
end
