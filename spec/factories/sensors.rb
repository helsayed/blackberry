FactoryBot.define do
  factory :sensor do
    name { Faker::Name.name }
    value { Faker::Name.name }
    device
  end
end
