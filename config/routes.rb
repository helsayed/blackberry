Rails.application.routes.draw do
  root to: "devices#index"
  resources :sensors
  resources :devices
  namespace :api do
    namespace :v1 do
      resources :devices
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
