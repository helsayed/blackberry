class Device < ApplicationRecord

  # association
  has_many :sensors, dependent: :destroy
  accepts_nested_attributes_for :sensors, allow_destroy: true

  # validation
  validates :name, presence: true
  validates :category, presence: true
end
