class SensorNotificationChannel < ApplicationCable::Channel
  def subscribed
     stream_from "sensor_notifications_channel"
   end

   def unsubscribed
   end
end
