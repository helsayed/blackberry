module Api
  module V1
    class DevicesController < ApiController

      include Broadcastable
      before_action :set_device, only: [:show, :update, :destroy]
      after_action :broadcast_sensor_state, only: :update


      # GET /devices.json
      def index
        @devices = Device.all
      end
      
      # GET /devices/1.json
      def show
      end

      # POST /devices.json
      def create
        @device = Device.new(device_params)

        respond_to do |format|
          if @device.save
            format.json { render :show, status: :created, location: @device }
          else
            format.json { render json: @device.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /devices/1.json
      def destroy
        @device.destroy
        respond_to do |format|
          format.json { head :no_content }
        end
      end

      # PATCH/PUT /devices/1.json
      def update
        respond_to do |format|
          if @device.update(device_params)
            format.html { redirect_to @device, notice: 'Device was successfully updated.' }
            format.json { render :show, status: :ok, location: @device }
          else
            format.html { render :edit }
            format.json { render json: @device.errors, status: :unprocessable_entity }
          end
        end
      end

      private

      def set_device
        @device = Device.find(params[:id])
      end

      def device_params
        params.require(:device).permit(:name, :category, sensors_attributes: [  :id, :name, :value ] )
      end

    end
  end
end
