module Broadcastable
  extend ActiveSupport::Concern
  
  def broadcast_sensor_state
    ActionCable.server.broadcast 'sensor_notifications_channel',
        message: render_message(@device.sensors)
  end

  private

  def render_message(sensors)
    ApplicationController.render(partial: 'sensors/sensors_table',
                                 locals: { sensors: sensors })
  end
end
