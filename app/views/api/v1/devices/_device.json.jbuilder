json.id device.id
json.name device.name
json.category device.category

json.sensors device.sensors do |sensor|
  json.id sensor.id
  json.name sensor.name
  json.value   sensor.value
end
