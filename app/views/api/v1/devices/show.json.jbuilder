json.name @device.name
json.category @device.category

json.sensors @device.sensors do |sensor|
  json.name sensor.name
  json.value   sensor.value
end
